"""Loss function for the FastFlow Model Implementation."""

# Copyright (C) 2022 Intel Corporation
# SPDX-License-Identifier: Apache-2.0

from typing import List

import torch
from torch import Tensor, nn
import numpy as np


class FastflowLoss(nn.Module):
    """FastFlow Loss."""

    def forward(self, hidden_variables: List[Tensor], jacobians: List[Tensor]) -> Tensor:
        """Calculate the Fastflow loss.

        Args:
            hidden_variables (List[Tensor]): Hidden variables from the fastflow model. f: X -> Z
            jacobians (List[Tensor]): Log of the jacobian determinants from the fastflow model.

        Returns:
            Tensor: Fastflow loss computed based on the hidden variables and the log of the Jacobians.
        """
        loss = torch.tensor(0.0, device=hidden_variables[0].device)  # pylint: disable=not-callable
        for (hidden_variable, jacobian) in zip(hidden_variables, jacobians):
        # Original loss    
        #    loss += torch.mean(0.5 * torch.sum(hidden_variable**2, dim=(1, 2, 3)) - jacobian)

        # L2 Loss
        #    l2_loss = torch.mean((torch.sum(hidden_variable**2, dim=(1,2,3)) - jacobian)**2)
        #    loss += l2_loss
        
        # l1 loss
        #    l1_loss = torch.mean(torch.abs(torch.sum(hidden_variable**2, dim=(1,2,3)) - jacobian))
        #    loss += l1_loss
        
        # Huber loss, delta=1
            huber_loss = torch.mean(torch.where(
                torch.abs(torch.sum(hidden_variable**2, dim=(1,2,3)) - jacobian) < 1, 
                0.5 * (torch.sum(hidden_variable**2, dim=(1,2,3)) - jacobian)**2, 
                torch.abs(torch.sum(hidden_variable**2, dim=(1,2,3)) - jacobian) - 0.5))
            loss += huber_loss

        return loss
        
